module.exports = function(){
    var faker = require("faker");
    var _ = require("lodash");
    return {
        avisos: _.times(1000, function (n) {
            return {
                id: n,
                name: faker.name.findName(),
                self: Math.random() < 0.1,
                avatar: faker.internet.avatar(),
                image: faker.image.image(),
                time: faker.date.recent(),
                message: faker.lorem.sentence()
            }
        }),

    }
}
