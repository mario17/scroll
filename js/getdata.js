/*
 * modulo paginacion
 * @clase pagination-get-data
 * @author Mariano 	Zamora - @marianozamora
 */

yOSON.AppCore.addModule('pagination-get-data', function(Sb){
	let dom, st, catchDom, suscribeEvents, event,fn, initialize;
	dom = {}
	st = {
		pathBase:'http://172.18.60.64:3001/avisos?_page='
	}
	catchDom = () => {
		/*dom.container = $(st.container);*/
	}
	suscribeEvents = () => {
		Sb.events( ['pagination-get-data:fetch'], fn.fetch, this );
	}

	event = {};

	fn = {};

	fn.addItem_ = (opt) => {
		opt.items_.push({
			'data': null,
			'node': null,
			'height': 0,
			'width': 0,
			'top': 0,
		})
	}

	fn.instanceFetch = (url) => {
		return fetch(url).then(val => val.json())
	}

	fn.fetch = (opt, pageForRequest) => {
		if(pageForRequest){
			pageForRequest.forEach(val=>{
				let url = `${st.pathBase}${val}`;
				fn.instanceFetch(url).then((value)=>{
					fn.addContent(value, opt);
				})
			})
		}		
	}

	fn.addContent = (items,opt) => {
		opt.requestInProgress_ = false;
		var startIndex = opt.items_.length;
		for (var i = 0; i < items.length; i++) {
			if(!opt.items_[items[i].id]){
				fn.addItem_(opt);
			}
			if(!opt.items_[items[i].id].data){
				opt.items_[items[i].id].data = items[i];
			}
		}
		Sb.trigger( ['pagination-attach:attachContent'], opt );
	}

	initialize=()=>{
		catchDom();
		suscribeEvents();
	};
	return{
		init: initialize
	}
});




