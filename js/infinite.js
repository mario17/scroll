/*
 * modulo paginacion
 * @clase pagination-cache
 * @author Mariano 	Zamora - @marianozamora
 */

yOSON.AppCore.addModule('pagination-infinite', function(Sb){
	let dom, st, catchDom, suscribeEvents, event,fn, initialize;
	dom = {}
	st = {
		lastScroll:0,
		scrollPosition:0,
		paginationElement:'.js-pagination-element',
		container:".container",
		header:"header"
	}
	let opt = {};
	const RUNWAY_ITEMS = 10;
	const RUNWAY_ITEMS_OPPOSITE = 10;
	const SCROLL_ANIMATION = 1000;

	catchDom = () => {
		dom.container = $(st.container);
		dom.header    = $(st.header);
	}

	afterCatchDom = () => {
		opt.anchorItem = {index: 0, offset: 0};
		opt.firstAttachedItem_ = 0;
		opt.lastAttachedItem_ = 0;
		opt.anchorScrollTop = 0;
		opt.tombstoneSize_ = 0;
		opt.tombstoneWidth_ = 0;
		opt.tombstones_ = [];
		opt.items_ = [];
		opt.loadedItems_ = 0;
		opt.requestInProgress_ = false;
		opt.scrollRunway_ = document.createElement('div');
		opt.scrollRunway_.textContent = ' ';
		opt.scrollRunwayEnd_ = 0;
		opt.scrollRunway_.style.position = 'absolute';
		opt.scrollRunway_.style.height = '1px';
		opt.scrollRunway_.style.width = '1px';
		opt.scrollRunway_.style.transition = 'transform 0.2s';
	}

	suscribeEvents = () => {
		Sb.events( ['pagination-infinite:InfiniteScroller'], fn.InfiniteScroller, this);
		dom.container.on('scroll', event.onScroll_);
		$(window).on('resize', event.onResize_);
	};

	fn = {};
	fn.getSizesTombstone = () => {
		var $tombstone = fn.createTombstone(opt.source_.tombstone_);
		$tombstone.css('position' ,'absolute');
		opt.canvas.append($tombstone);
		$tombstone.removeClass('.invisible')

		opt.tombstoneSize_ = $tombstone.outerHeight();
		opt.tombstoneWidth_ = $tombstone.outerWidth();
		opt.canvas.children('.tombstone').remove();
	}
	
	fn.initializeTombstoneSizes = () => {
		for (var i = 0; i < opt.items_.length; i++) {
			opt.items_[i].height = opt.items_[i].width = 0;
		}
	}

	event = {}
	event.onResize_ = () => {
		fn.getSizesTombstone();
		fn.initializeTombstoneSizes();
		event.onScroll_();
	}

	event.onScroll_ = () => {

		//set first values
		let delta = opt.canvas.scrollTop() - opt.anchorScrollTop;
		let defaultAnchoreItem = {
			index: 0, 
			offset: 0
		}

		//calculate anchored per item in canvas
		opt.anchorItem = (opt.canvas.scrollTop()) ? fn.calculateAnchoredItem(opt.anchorItem, delta) : defaultAnchoreItem;
		opt.anchorScrollTop = opt.canvas.scrollTop();
		let lastScreenItem = fn.calculateAnchoredItem(opt.anchorItem, opt.canvas.outerHeight());
		let startIndex = (delta < 0) ? opt.anchorItem.index - RUNWAY_ITEMS : opt.anchorItem.index - RUNWAY_ITEMS_OPPOSITE;
		let lastIndex = (delta < 0) ? lastScreenItem.index + RUNWAY_ITEMS_OPPOSITE : lastScreenItem.index + RUNWAY_ITEMS;
		
		//calcuate lastIndex 
		lastItemIndex = opt.lastPage * opt.quantityPerPage;

		//define first and start index
		startIndex = Math.min(startIndex,lastItemIndex);
		lastIndex = Math.min(lastIndex,lastItemIndex)
		
		//Fill canvas with tombstone or data
		fn.fill(startIndex, lastIndex);

		//Hide header with scroll
		fn.initialAnimation();
	}

	fn.initialAnimation = () =>{
		if(dom.container.scrollTop() > dom.header.outerHeight())
			$(window).scrollTop(dom.header.outerHeight());
	}
	fn.cleaningUrl = (str) => {
		return str.replace(/[0-9]+$/,'');
	}

	fn.getUrlFromHref = (element, callback) => {
		return $(element)[callback]().children().attr('href');
	}

	fn.generateUrlRequest  = (element, callback) => {
		let str = fn.getUrlFromHref(element, callback);
		let url = fn.cleaningUrl(str);
		return url;
	}

	fn.setUtils = () => {
		let url = dom.container.data('url');
		let lastPage = fn.getUrlFromHref(st.paginationElement,'last');
		opt.urlTemplate = fn.cleaningUrl(url);
		opt.initialPage = fn.getPageNumber(url);
		opt.lastPage = fn.getPageNumber(lastPage);
		fn.scrollToPage(dom.container);
	}

	fn.scrollToPage = (container) => {
		container.animate({
			scrollTop: 270*10*(opt.initialPage-1)
		}, 1000, function(){
			Sb.trigger( ['pagination-attach:calculatingViewport'], opt );
		});
	}
	fn.getPageNumber = (element) => {
		let pageNumber = parseInt(element.match(/[0-9]+$/))/**/
		return isNaN(pageNumber) ? 1 : pageNumber
	}


	fn.scrollPosition = (position) =>{
		return st.positionScroll = $(window).scrollTop();
	}


	fn.InfiniteScroller = function(canvas, source) {
		opt.canvas  =  canvas;
		opt.source_ = source;
		opt.quantityPerPage = source.quantityPerPage;
		opt.canvas.append(opt.scrollRunway_);
		event.onResize_();
	}

	fn.createTombstone = (element) =>{
		return element.clone();
	}

	fn.calculateAnchoredItem =(initialAnchor, delta) =>{
		//init Anchor per item
		if (delta == 0)
			return initialAnchor;
		delta += initialAnchor.offset;
		let i = initialAnchor.index;
		let tombstones = 0;

		//calculate with delta
		if (delta < 0) {
			//less scroll up
			while (delta < 0 && i > 0 && opt.items_[i - 1].height) {
				delta += opt.items_[i - 1].height;
				i--;
			}
		  	tombstones = Math.max(-i, Math.ceil(Math.min(delta, 0) / opt.tombstoneSize_));
		} else {
			//more scroll down
			while (delta > 0 && i < opt.items_.length && opt.items_[i].height && opt.items_[i].height < delta) {
				delta -= opt.items_[i].height;
				i++;
			}
			if (i >= opt.items_.length  || !opt.items_[i].height)
				tombstones = Math.floor(Math.max(delta, 0) / opt.tombstoneSize_);
		}

		i += tombstones;
		delta -= tombstones * opt.tombstoneSize_;

		return {
		  index: i,
		  offset: delta,
		};
	}


	fn.fill = (start, end) => {
		opt.firstAttachedItem_ =  Math.max(0, start);
		opt.lastAttachedItem_ = end;
		Sb.trigger( ['pagination-attach:attachContent'], opt );
	}


	fn.addItem_ = () => {
	  opt.items_.push({
		'data': null,
		'node': null,
		'height': 0,
		'width': 0,
		'top': 0,
	  })
	}



	initialize=()=>{
		console.log("[RUN INFINITE.JS]");
		catchDom();
		fn.setUtils();
		afterCatchDom();
		suscribeEvents();
	};
	return{
		init: initialize
	}
});

