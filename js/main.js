
/*
 * modulo paginacion
 * @clase pagination-cache
 * @author Mariano  Zamora - @marianozamora
 */

yOSON.AppCore.addModule('pagination-next', function(Sb){
	let dom, st, event,fn, initialize;
	dom = {}
	st = {
		containerTemplates : "#templates",
		tombstone          : ".js-tombstone",
		template           : ".js-card-item",
		canvas		       : ".container"
	};
	const ITEMPERPAGE = 10;
	
	catchDom = () => {
		dom.containerTemplates = $(st.containerTemplates);
		dom.tombstone          = $(st.tombstone, dom.containerTemplates);
		dom.template           = $(st.template, dom.containerTemplates);
		dom.canvas             = $(st.canvas);
	};
	event = {}

	fn = {}

	fn.ContentSource = () =>{
		let obj = {};
		obj.tombstone_ = dom.tombstone;
		obj.messageTemplate_ = dom.template;
		obj.render = fn.renderTemplate;
		obj.quantityPerPage = ITEMPERPAGE;
		return obj;
	}


	fn.renderTemplate = (item, div)  => {
		div = div ||  dom.template.clone();
		div.attr('data-id', item.id);
		div.attr('data-page', item.page);
		div.attr('data-url', item.url);
		div.find('.b-card-tag').text('Element');
		div.addClass('b-card-item--premium');
		div.find('.b-card-image-slider  img').attr('src', item.avatar);
		div.find('.b-card-description .b-card-title').text(item.message);
		div.find('.posted-date').text(item.time.toString());

		var img = div.children('.bubble img');

		if(item.image) {
			img.removeClass('invisible');
			img.attr('src', item.image.src);
			img.attr('width', item.image.width);
			img.attr('height', item.image.height);
			return div
		}
		img.attr('src','');
		img.addClass('invisible');
		return div;
	}


	fn.init = () => {
		let ContentSource = fn.ContentSource();
		console.log("['pagination-infinite:InfiniteScroller'] trigger");
		Sb.trigger( ['pagination-infinite:InfiniteScroller'], dom.canvas, ContentSource);
	}

	initialize=()=>{
		console.log("[RUN MAIN.JS]");
		catchDom();
		fn.init();
	};
	return{
		init: initialize
	}
});



