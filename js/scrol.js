let App = ((Sb) =>  {
	let fn,initialize,st,dom;
	fn = {};
	st = {
		container:".js-results-of-search",
		headerMobile:'.js-header-responsive-main',
		last_scroll :0,
		flag:0,
		type:'',
		url:'http://localhost:3004/avisos?_page=1'
	}
	dom = {}
	catchDom = () => {
		dom.container = $(st.container);
		dom.headerMobile = $(st.headerMobile);
	}
	subscribeEvents = () => {
		$(window).on('scroll', fn.initApp);
	}
	events = {
	}
	fn.middleOfPage = () =>{
		let scroll_pos = $(window).scrollTop();
		if (Math.abs(scroll_pos - st.last_scroll)>$(window).height()*0.1) {
			return 'MOSTLYVISIBLE'
		}
	}
	fn.changeUrl = (container) => {
		let scroll_pos = $(window).scrollTop();
		st.last_scroll = scroll_pos;
		console.log(container);
      container.each(function(index) {
        if (fn.mostlyVisible(this)) {
          history.replaceState(null, null, $(this).attr("data-url"));
          $("#pagination").html($(this).attr("data-pagination"));
          return(false);
        }
      });
	}

	fn.mostlyVisible = (element) =>{
		let scroll_pos = $(window).scrollTop();
		let window_height = $(window).height();
		let element_top = $(element).offset().top;
		let element_height = $(element).height();
		let element_bottom = element_top + element_height;
		let total = ((element_bottom - element_height*0.25 > scroll_pos) && (element_top < (scroll_pos+0.5*window_height)));
		return 'MOSTLYVISIBLE';
	}

	fn.initApp = () => {
		fn.middleware(fn.calculateType());
		fn.changeUrl(dom.container);
	}

	fn.calculateNext = (flag) =>{
		let scroll_pos = $(window).scrollTop();
		if (scroll_pos >= 0.9*($(document).height() - $(window).height())) {
			return 'NEXT'
		}
	}

	fn.calculatePreview = () =>{
		let scroll_pos = $(window).scrollTop();
		if ( scroll_pos <= 0.9*dom.headerMobile.height()) {
			return 'PREVIEW'
		}
	}

	fn.calculateType = () => {
		let type = fn.calculateNext() || fn.calculatePreview() || fn.middleOfPage() ;
		return type;
	}

	fn.middleware = (type) => {
		switch(type){
			case 'NEXT':
				console.log('1');
				$(window).off('scroll');
				fn.nextPage()
				break;
			case 'PREVIEW':
				console.log('2');
				$(window).off('scroll');
				fn.previewPage()
				break;
		}
	}

	fn.nextPage = (el) => {
		fn.getData(st.url,{method:'GET'});
	}

	fn.previewPage = (el) => {
		setTimeout(()=>{
				console.log("termino de traer la anterior pagina");
				$(window).on('scroll', fn.initApp);
		}, 3000);
	}
	fn.getPageNumber = () => {
		let pageNumber = parseInt(location.search.match(/[0-9]/))
		return isNaN(pageNumber) ? 1 : pageNumber
	}

	fn.validateCache = (page) =>{
		let pageNumberCache = localStorage.getItem(`?_page=${page}`)
		pageNumberCache != null ?  pageNumberCache : null
	};
	fn.saveCache = () =>{
	};	
	
	fn.manageData = () => {
		fn.validateCache(fn.getPageNumber());
	}
	fn.getData = (url,settings) =>{
		fetch(`${url}`,settings).then(function(response) {
		 return response.json(); 
		 }).then(function(data) {
			let dataParsed = JSON.stringify(data);
			localStorage.setItem('?_page=1',dataParsed)
			$(window).on('scroll', fn.initApp);
		})

	}
	initialize = () => {
		catchDom();
		subscribeEvents();
		fn.manageData();
		console.log(dom.headerMobile)

	};
	return {
		init: initialize
	};
})();
App.init();

