
/*
 * modulo paginacion
 * @clase pagination-cache
 * @author Mariano 	Zamora - @marianozamora
 */

yOSON.AppCore.addModule('pagination-attach', function(Sb){
	let dom, st, catchDom, suscribeEvents, event,fn, initialize;
	dom = {}
	st = {
		lastScroll:0
	}
	let final_page = 10;
	const SCROLL_RUNWAY = 0;
	const ANIMATION_DURATION_MS = 200;
	let pageTotal = new Set()

	catchDom = () => {
	}

	suscribeEvents = () => {
		Sb.events( ['pagination-attach:attachContent'], fn.attachContent, this );//calculatingViewport
		Sb.events( ['pagination-attach:calculatingViewport'], fn.calculatingViewport,  this );
	}
	
	event = {}

	fn = {}

	//zone tombstone

	fn.createTombstone = (element) =>{
		return element.clone();
	}

	fn.getTombstone = (opt) => {
		var tombstone = opt.tombstones_.pop();
		if (tombstone ) {
			tombstone.removeClass('invisible');
			tombstone.css('opacity', 1);
			tombstone.css('transform ', '');
			tombstone.css('transition' ,'');
			return tombstone;
		}
		return fn.createTombstone(opt.source_.tombstone_);

	}

	fn.addItem_ = (opt) => {
		opt.items_.push({
			'data': null,
			'node': null,
			'height': 0,
			'width': 0,
			'top': 0,
		})
	}
	// End zone tombstone


	fn.calculatingViewport = (opt) => {
		let arr;
		setInterval(()=>{
			arr = getitemsIntoViewport();
			let pagesRequest = new Set();
			arr.each((i, item)=>{
				let pageForRequest = parseInt($(item).attr('data-page'));
				fn.replaceUrl(opt, pageForRequest);
				if(!pageTotal.has(pageForRequest)){
					opt.requestInProgress_ = true;
					pageTotal.add(pageForRequest);
					pagesRequest.add(pageForRequest);
					Sb.trigger( ['pagination-get-data:fetch'], opt,pagesRequest );

				}
			})
			pagesRequest.clear();
		},1050)
	}

	fn.replaceUrl = (opt,val) => {
		let { urlTemplate } = opt;
		history.replaceState(null, null, `${urlTemplate}${val}` );
	}

	getitemsIntoViewport = () => {
		let viewport = getPositionTopBottomViewport();
		let items = $('.container .b-card-item:not(.invisible)');
		for (var i = items.length - 1; i >= 0; i--) {
			let $items = $(items[i]);
			let topE = $items.offset().top;
			let bottomE = $items.offset().top + $items.height();
			let validateTop = (topE > viewport.top) && (topE < viewport.bottom);
			let validateBottom = (bottomE > viewport.top) && (bottomE < viewport.bottom);
			if ( !validateTop && !validateBottom ) {
				items.splice(i,1);
			}
		}
		return items;
	}


	getPositionTopBottomViewport = () => {
		let heigthWindow = window.innerHeight;
		let topWindow = $(window).scrollTop();
		let bottomWindow = topWindow + heigthWindow;
		return { "top": topWindow, "bottom": bottomWindow };
	}


	fn.attachContent = (opt) => {
		let i;
		let  unusedNodes = [];
		let firstAttachelement;
		let lastAttachedItem;

		// 1st:  Skip the items which should be visible.
		for (i = 0; i < opt.items_.length; i++) {
			if (i == opt.firstAttachedItem_) {
				i = opt.lastAttachedItem_ - 1;
				continue;
			}
			if(opt.items_[i].node) {
				if (opt.items_[i].node.hasClass('tombstone')) {
					opt.tombstones_.push(opt.items_[i].node);
					opt.tombstones_[opt.tombstones_.length - 1].addClass('invisible');
				} else {
					unusedNodes.push(opt.items_[i].node);
				}
			}
			opt.items_[i].node = null;
		}


		// 2nd:  Create DOM nodes.

		let  tombstoneAnimations = {};
	
		for (i = opt.firstAttachedItem_; i < opt.lastAttachedItem_; i++) {
			while (opt.items_.length <= i)
				fn.addItem_(opt);
			if (opt.items_[i].node) {
				// if it's a tombstone but we have data, replace it.
				if (opt.items_[i].node.hasClass('tombstone') && opt.items_[i].data) {
					if (ANIMATION_DURATION_MS) {
						opt.items_[i].node.css('z-index', 1);
						tombstoneAnimations[i] = [opt.items_[i].node, opt.items_[i].top - opt.anchorScrollTop];
					} else {
						opt.items_[i].node.addClass('invisible');
						opt.tombstones_.push(opt.items_[i].node);
					}
					opt.items_[i].node = null;
				} else {
					continue;
				}
			}

			let node;

			if(opt.items_[i].data) {
				node =  opt.source_.render(opt.items_[i].data, unusedNodes.pop());
			} else {
				node = fn.getTombstone(opt);
			} 

			node.css('position' ,'absolute');
			opt.items_[i].top = -1;
			opt.canvas.append(node);
			opt.items_[i].node = node;

		}


		// Remove all unused nodes
		while (unusedNodes.length) {
			unusedNodes.map( 
				value => {
					let id =  value.data('id');
					opt.canvas.find(`[data-id=${id}]`).remove();
					unusedNodes.pop()
				});
		}


		// Get the height of all nodes which haven't been measured yet.
		for (i = opt.firstAttachedItem_; i < opt.lastAttachedItem_; i++) {
			// Only cache the height if we have the real contents, not a placeholder.
			if (opt.items_[i].data && !opt.items_[i].height) {
				opt.items_[i].height = opt.items_[i].node.outerHeight();
				opt.items_[i].width = opt.items_[i].node.outerWidth();
			}
		}


		// Fix scroll position in case we have realized the heights of elements
		// that we didn't used to know.
		opt.anchorScrollTop = 0;
		for (i = 0; i < opt.anchorItem.index; i++) {
			opt.anchorScrollTop += opt.items_[i].height || opt.tombstoneSize_;
		}
		opt.anchorScrollTop += opt.anchorItem.offset;




		// Position all nodes.
		let curPos = opt.anchorScrollTop - opt.anchorItem.offset;
		i = opt.anchorItem.index;
		while (i > opt.firstAttachedItem_) {
			curPos -= opt.items_[i - 1].height || opt.tombstoneSize_;
			i--;
		}
		while (i < opt.firstAttachedItem_) {
			curPos += opt.items_[i].height || opt.tombstoneSize_;
			i++;
		}


		// Set up initial positions for animations.
		for ( i in tombstoneAnimations) {
			let anim = tombstoneAnimations[i];
			opt.items_[i]
				.node
				.css({'transform ':`translateY(${opt.anchorScrollTop + anim[1]}px) scale(${opt.tombstoneWidth_ / opt.items_[i].width} , ${opt.tombstoneSize_ / opt.items_[i].height})`});
				// Call offsetTop on the nodes to be animated to force them to apply current transforms.
			opt.items_[i].node.offset().top;
			anim[0].offset().top;
			opt.items_[i].node.css('transition', `transform   ${ANIMATION_DURATION_MS}ms`);
		}
		
		for (i = opt.firstAttachedItem_; i < opt.lastAttachedItem_; i++) {
			var anim = tombstoneAnimations[i];

			if (anim) {
				anim[0].css({'transition':`transform  ${ANIMATION_DURATION_MS}ms  ease-in-out`});
				anim[0].css({'transform ': `translateY(${curPos}px) scale( ${opt.items_[i].width / opt.tombstoneWidth_} ,  ${opt.items_[i].height / opt.tombstoneSize_} )`});
				anim[0].css('opacity' ,0);//  translateY(0px) scale(1, 1.17647)
			}

			if (curPos != opt.items_[i].top) {
				if (!anim)
					opt.items_[i].node.css('transition' ,'');

				opt.items_[i].node.attr('data-url', opt.urlTemplate);
				let id = curPos/270;
				let page = Math.floor(id/10)+1;
				opt.items_[i].node.attr('data-id', id);
				opt.items_[i].node.attr('data-page', page);
				console.log(i,'INDEX', curPos,'CURRENT POSITION', opt)
				opt.items_[i].node.css({'transform' : `translateY(${curPos}px)`});
			}
			opt.items_[i].top = curPos;
			curPos += opt.items_[i].height || opt.tombstoneSize_;
		}

		//opt.scrollRunwayEnd_ = Math.max(opt.scrollRunwayEnd_, curPos + SCROLL_RUNWAY);
		opt.scrollRunwayEnd_ = opt.initialPage*270*10;
		$(opt.scrollRunway_).css({'transform': `translate(0, ${opt.scrollRunwayEnd_}px)`});


		if (ANIMATION_DURATION_MS) {
			setTimeout(function() {
				for (var i in tombstoneAnimations) {
					var anim = tombstoneAnimations[i];
					anim[0].addClass('invisible');
					opt.tombstones_.push(anim[0]);
				}
			}.bind(opt), ANIMATION_DURATION_MS)
		}
	}



	initialize=()=>{
		catchDom();
		suscribeEvents();
	};
	return{
		init: initialize
	}
});




